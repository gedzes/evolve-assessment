//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DPDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Input
    {
        public int InputID { get; set; }
        public string InputData { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FormulaID { get; set; }
        public bool IsInputValid { get; set; }
        public int ImportID { get; set; }
        public int importInputTypeID { get; set; }
    
        public virtual Formula Formula { get; set; }
        public virtual Import Import { get; set; }
        public virtual Result Result { get; set; }
    }
}
