﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataProcessingSolution.Models
{
	public class ResultModel
	{
		public string ImputData { get; set; }
		public string FormulaName { get; set; }
		public string FormulaRule { get; set; }
		public bool isInputValid { get; set; }
		public decimal Results { get; set; }
	}
}