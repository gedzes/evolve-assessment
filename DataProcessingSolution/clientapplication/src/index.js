﻿import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import UploadFile from './ClientApp';
import 'bootstrap/dist/css/bootstrap.css'; 




ReactDOM.render(<UploadFile />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more abou t service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
