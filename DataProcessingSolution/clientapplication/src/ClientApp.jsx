﻿import React, { Component } from 'react';
import axios, { post } from 'axios';
import $ from 'jquery';

function manageFormulas(props) {
	if (props == 'upload') {

	
	} else {
		return "";
	}
}


class UploadFile extends Component {
      //state = {}
      constructor(props) {
        super(props);
        this.state = {
          
		  }
		  
	}



	//showing component
	showComponent(componentName) {
		
		this.setState({ displayedTable: componentName });
		
	}
	//saving Formula
	SaveFormula() {
		axios.post('http://localhost:51356/api/Formular/', {
			FormulaName: document.getElementById('txtFormulaName').value,
			FormulaRule: document.getElementById('txtFRule').value
		})
			.then(function (response) {
				document.getElementById('fResponse').innerHTML = response.statusText;
				document.getElementById('txtFormulaName').value = '';
				document.getElementById('txtFRule').value = '';
			})
			.catch(function (error) {
				document.getElementById('fResponse').innerHTML = error;
;
			});
	}

	//Loading results
	GetData() {
		
		axios.get('http://localhost:51356/api/Computation/getResultByImport',null,null)
			.then(function (respose) {
				console.log(respose.data);
				 $('#tblBody').empty();
				$.each(respose.data, function (index, rslt) {
					var row = $('<tr><td>' + rslt.ImputData + ' </td><td> ' + rslt.isInputValid + ' </td><td>'
						+ rslt.FormulaName + ' </td><td> ' + rslt.FormulaRule + ' </td><td> ' + rslt.Results + '</td></tr > ');

				
					$('#tblData').append(row);

				});
				if (respose.data.length == 0) {

					document.getElementById('lResults').innerHTML = 'NO Data';
				}
				
			})
			.catch(function (error) {
				document.getElementById('divError').innerHTML = error;
				
			});
	}
	
	//on load of the file
  onChange(e) {
  
     
    let files = e.target.files;
    let reader = new FileReader();
     reader.readAsText(files[0]);

	  reader.onload = (e) => {
		  

		let d = e.target.result;
		axios.post('http://localhost:51356/api/Computation/AddcomputeData',null,{
			params: {
				inputData: d,
				fName: document.getElementById('ufile').value
				
			}
		})
			.then(function (response) {
				
				document.getElementById('divError').innerHTML = "File processed successfuly. ";
				setTimeout(function () { window.location.reload(); }, 2000);
				
			})
			.catch(function (error) {
				console.log(error);
				document.getElementById('divError').innerHTML = "Invalid Input "+ error.statusText;
				
			});


    }

    reader.onloadend = (e) => {
      //const t = reader.readAsText(files[0]);
      //document.getElementById('divError').innerHTML = t;
    }
 

    reader.onerror = (e) => {
      const errorText = reader.result;
      document.getElementById('divError').innerHTML = errorText;
    }


  }



	render() {
		const components = {
			
			"upload": <div className="container" style={{ paddingTop: '20px' }}>


				<div className="row">

					<div className="col-md-12" >


						<div onSubmit={this.onFormSubmit}>

							<h3> Please Upload File </h3 >
							<input type="file" accept=".txt"  id="ufile" className="btn btn-primary" name="ufile" onChange={(e) => this.onChange(e)} />

						</div>

					</div >
				</div>
				<div className="" id="divError">
					<div id="errorText">

					</div>
				</div>
			</div>,
			"Formula": <div id ="formula" class="col-md-10 col-md-offset-1 " style={{marginTop: '20px'}}>
				<div class="well">
					<table class="table table-bordered ">
						<thead>
							<tr class="success">
								<td colspan="2">
									Add New Formular
								
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Formula Name</td>
								<td>
									<input type="text" id="txtFormulaName" placeholder="FormulaName" />
								</td>

							</tr>

							<tr>
								<td>Formula Rule</td>
								<td>
									<input type="text" id="txtFRule" placeholder="Formula Rule" />
								</td>

							</tr>
							
							<tr class="success">

								<td colspan="2">
									<input type="button" onClick={() => this.SaveFormula()} value="Save Formula" id="btnSaveFormula" class="btn btn-success "  />
								</td>

							</tr>


						</tbody>
					</table>

					<div id="fResponse" >
						
						
					</div>
				</div>
			
			</div>,
			"GetResult": <div id="getRID" class="col-md-10 col-md-offset-1" style={{ marginTop: '20px' }}>
			
				<div id="divData" class="well">
					<table class="table table-bordered" id="tblData">
						<thead class="success">
							<tr>
								<th>InputData</th>
								<th>IsInputValid</th>
								<th>Formula Name</th>
								<th>Formula Rule</th>
								<th>Result</th>

							</tr>

						</thead>
						<tbody id="tblBody"></tbody>
					</table>
					<div class="success" style={{ marginTop: '20px' }}>

						
						<input type="button" onClick={() => this.GetData()} value="Load Latest Results" id="btnGetResults" class="btn btn-success " />
						

					</div>
				</div>
				<div class="" id="lResults">


				</div>
			</div>
				

		}
	  return (
		  <div className='container' style={{ paddingTop: '20px' }} >
			  <div className="row">
				  
				  <div className="col-md-12" >
					  <h5>Evolve  Assessment</h5>
					  <button className='btn btn-success ' onClick={() => this.showComponent('upload')}>Upload Files</button>
					  <button style={{ marginLeft: '20px' }} className='btn btn-success ' onClick={() => this.showComponent('Formula')}>Manage Formulas</button>
					  <button style={{ marginLeft: '20px' }} className='btn btn-success ' onClick={() => this.showComponent('GetResult')}>Get Results</button>

				  </div >
			  </div>
			{components[this.state.displayedTable]}
		</div>

    )


  }

       
}
export default UploadFile;