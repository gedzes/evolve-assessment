﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(DataProcessingSolution.Startup))]

namespace DataProcessingSolution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
			System.Diagnostics.Process.Start("http://localhost:3001");
		}
    }
}
