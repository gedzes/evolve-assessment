﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DPDataAccess;


namespace DataProcessingSolution.Controllers
{
    public class FormularController : ApiController
    {
        
        public HttpResponseMessage AddFormular([FromBody] Formula formula)
        {
			using (EvolveDBEntities entities = new EvolveDBEntities())
			{
				try
				{
					entities.Formulas.Add(formula);
					entities.SaveChanges();
					var message = Request.CreateResponse(HttpStatusCode.Created, "Saved Successfully");
					return message;
				}
				catch (Exception ex)
				{

					return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
				}


			}




        }
    }
}
