﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text.RegularExpressions;
using DPDataAccess;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using DataProcessingSolution.Models;
using System.Web.Http.Cors;


namespace DataProcessingSolution.Controllers
{
    [EnableCorsAttribute("*","*","*")]
	[RoutePrefix("api/Computation")]
	public class ComputationController : ApiController
	{
		EvolveDBEntities entities = new EvolveDBEntities();
		private string[] splitLines;
		private int batch = 1500;
		private int latestImportID = 0;
		private DataTable inputDT = new DataTable();
		private int FormularID = 0;

		[HttpPost]
		[Route("AddcomputeData")]
		public async Task<HttpResponseMessage> AddcomputeData(string inputData, string fName)
		{
			try
			{
				if (Path.GetExtension(fName) != ".txt")
				{
					return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "invalid file type. Please upload only Text files");
				}

				if (inputData.Length > 0)
				{

				
				var pattern = @"[\r\n]+";
				var replacedString = Regex.Replace(inputData, pattern, "*");
				splitLines = replacedString.Split('*');
				latestImportID = AddNewImport(fName);
				Task task = new Task(ValidateAndSaveData);
				task.Start();
				var msg = Request.CreateResponse("Processing File. Please wait.....");
				await task;
				msg = Request.CreateResponse("File Processed Successfully");
				return msg;
					
				}
				else
				{
					return Request.CreateErrorResponse(HttpStatusCode.BadRequest,"Invalid input");
				}

			}
			catch (Exception ex)
			{

				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}

			//var lines;//= OpenAndValidateFile(documentStream);


		}

		[HttpGet]
		[Route("getResultByImport")]
		public List<ResultModel> getResultByImport()
		{
			
			try
			{
				int latestImportID = 0;
				List<ResultModel> resultsModel = new List<ResultModel>();
				using (EvolveDBEntities ee = new EvolveDBEntities())
				{
					if (ee.Imports.Count() > 0)
					{
                      latestImportID = ee.Imports.Max(c => c.ImportID);
					}
					else
					{
						return resultsModel;	
					}

					 

				}
				string conString = ConfigurationManager.ConnectionStrings["EvolveDbCon"].ConnectionString;
				using (SqlConnection con = new SqlConnection(conString))
				{

					using (SqlCommand cmd = new SqlCommand(conString))
					{
						
						SqlParameter dataTableParameter = new SqlParameter();
						cmd.CommandText = "dbo.cmdGetResultsByImport";
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.Parameters.AddWithValue("@ImportID", latestImportID);
						cmd.Connection = con;
						con.Open();
						using (SqlDataReader sreader = cmd.ExecuteReader())
						{

							while (sreader.Read())
							{
								ResultModel rm = new ResultModel()
								{
									ImputData = sreader["InputData"].ToString(),
									isInputValid = Convert.ToBoolean(sreader["isInputValid"]),
									FormulaName = sreader["FormulaName"].ToString(),
									FormulaRule = sreader["FormulaRule"].ToString(),
									Results = Convert.ToDecimal(sreader["Result"] == DBNull.Value? 0: sreader["Result"])

								};
                                resultsModel.Add(rm);
							}
						}
						

					}

				}
				return resultsModel;
			}
			catch (Exception ex)
			{
				throw new Exception(" error occured while trying to fetch results" + ex.ToString());

			}
		}

		private int AddNewImport(string fileName)
		{
			try
			{
				var newImport = new Import() { FileName = fileName, ImportDate = DateTime.Now };
				using (EvolveDBEntities evolve = new EvolveDBEntities())
				{
					evolve.Imports.Add(newImport);
					evolve.SaveChanges();
					return entities.Imports.Max(c => c.ImportID);
				}
			}
			catch (Exception ex)
			{

				throw new Exception("Error adding new import " + ex.ToString());
			}




		}



		private void ValidateAndSaveData()
		{
			 inputDT = setUpDataTable();

			if (splitLines.Length < batch)
			{
				for (int i = 1; i <= splitLines.Length; i++)
				{
					var data = splitLines[i-1];
					var split = data.Split(';').ToList();
					bool isFormulaValid = validateFormular(split);

					//processing valid data
					if ( CheckIfAllArrayItemsAreInts(split) & split.Count() == 4 & isFormulaValid)
					{
						int Value1 = Convert.ToInt32(split[1]);
						int Value2 = Convert.ToInt32(split[2]);
						int Value3 = Convert.ToInt32(split[3]);

						PopulateDataSet(  data, i, FormularID, Value1, Value2, Value3, true);
						
					}
					else
					{

						PopulateDataSet( data, i, 0, 0, 0, 0, false);
						
					}


				}

				CommitToDB(inputDT);



			}
			else
			{
				for (int i = 1; i <= splitLines.Length; i++)
				{
					var data = splitLines[i - 1];
					var split = data.Split(';').ToList();
					bool isFormulaValid = validateFormular(split);


					//processing valid data
					if (CheckIfAllArrayItemsAreInts(split) & split.Count() == 4 & isFormulaValid)
					{
						int Value1 = Convert.ToInt32(split[1]);
						int Value2 = Convert.ToInt32(split[2]);
						int Value3 = Convert.ToInt32(split[3]);

						PopulateDataSet(data, i, FormularID, Value1, Value2, Value3, true);

					}
					else
					{

						PopulateDataSet(data, i, 0, 0, 0, 0, false);

					}

					if (inputDT.Rows.Count == batch)
					{

						CommitToDB(inputDT);
						inputDT.Dispose();
						inputDT = setUpDataTable();
						GC.Collect();
					}

				}

				if (inputDT.Rows.Count > 0)
				{
					CommitToDB(inputDT);
					inputDT.Dispose();
					inputDT = null;
					GC.Collect();
				}

			}




		}

		private bool validateFormular (List<string> items)
		{
			int outForm = 0;
			//checking if the first item is a integer or not
			bool isFormulaNumber = items.Count() == 4 ? int.TryParse(items[0], out outForm) : false;
			//checking if the first item is a valid formula or if it exist in the DB
			bool isValidFormula = isFormulaNumber ? isFormulaValid(Convert.ToInt32(items[0])) : false;

			if (isValidFormula)
			{
				FormularID = Convert.ToInt32(items[0]);
			}
			return isValidFormula;
		}

		private void PopulateDataSet( string data ,int iterator, int FormulaID, int value1, int value2, int value3, bool IsInputValid)
		{
			DataRow dr =  inputDT.NewRow();
			dr["importInputTypeID"] = iterator;
			dr["InputData"] = data;
			if (FormulaID > 0)
			{
               dr["FormulaID"] = FormulaID;
			}
			dr["CreatedDate"] = DateTime.Now;
			dr["IsInputValid"] = IsInputValid;
			dr["ImportID"] = latestImportID;
			dr["Value1"] = value1;
			dr["Value2"] = value2;
			dr["Value3"] = value3;

		    inputDT.Rows.Add(dr);
		}

		private bool CheckIfAllArrayItemsAreInts(List<string> list)
		{

			int outParam = 0;
			return list.All(c => int.TryParse(c.ToString(), out outParam));
			 
		}

		private void CommitToDB(DataTable dataTable)
		{
			try
			{
				string conString = ConfigurationManager.ConnectionStrings["EvolveDbCon"].ConnectionString;
				using (SqlConnection con = new SqlConnection(conString))
				{
					
					using (SqlCommand cmd = new SqlCommand(conString))
					{
						cmd.Connection = con;
						con.Open();
						SqlParameter dataTableParameter = new SqlParameter();
						cmd.CommandText = "dbo.cmdImportInputData";
						cmd.CommandType = CommandType.StoredProcedure;
						dataTableParameter = cmd.Parameters.AddWithValue("@ImportInputDataTable", dataTable);
						dataTableParameter.SqlDbType = SqlDbType.Structured;
						dataTableParameter.TypeName = "dbo.ImportInputType";
						cmd.Parameters.AddWithValue("@ImportID", latestImportID);
						cmd.ExecuteNonQuery();

					}

				}
			}
			catch (Exception ex)
			{
				throw new Exception(" error occured while trying to save data to the db" + ex.ToString() );
				
			}
			
		}

		private bool isFormulaValid(int formulaID)
		{
			return entities.Formulas.Any(c => c.FormulaID == formulaID);
		}
		private DataTable setUpDataTable()
		{
			DataTable dtInput = new DataTable();
			dtInput.Columns.Add("importInputTypeID", typeof(int));
			dtInput.Columns.Add("InputData", typeof(string));
			dtInput.Columns.Add("CreatedDate", typeof(DateTime));
			dtInput.Columns.Add("FormulaID", typeof(int));
			dtInput.Columns.Add("IsInputValid", typeof(bool));
			dtInput.Columns.Add("ImportID", typeof(int));
			dtInput.Columns.Add("Value1", typeof(decimal));
			dtInput.Columns.Add("Value2", typeof(decimal));
			dtInput.Columns.Add("Value3", typeof(decimal));


			return dtInput;


		}

		
	}


}
